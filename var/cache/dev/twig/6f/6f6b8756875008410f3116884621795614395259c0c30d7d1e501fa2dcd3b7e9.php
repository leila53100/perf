<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/_form.html.twig */
class __TwigTemplate_c4c78bdd9e133ee3830dc153c15b3f5ae489c978e7582a017f03716ff14ebdf7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/_form.html.twig"));

        // line 1
        echo "
";
        // line 2
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 2, $this->source); })()), 'form_start');
        echo "
  
     <div class=\"container\">
        
        <div >";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), "name", [], "any", false, false, false, 6), 'row');
        echo "</div>
        
        <div>";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "description", [], "any", false, false, false, 8), 'row');
        echo "</div>   

        <div class=\"row\">
          <div class=\"col-md-6\">";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "niveau", [], "any", false, false, false, 11), 'row');
        echo "</div>
          <div class=\"col-md-6\">";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "trick_group", [], "any", false, false, false, 12), 'row');
        echo "</div>
        </div>
        <div class=\"row\">
          
          <div class=\"col-md-6\">
            <ul id=\"img-fields-list\"
            data-prototype=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "imgDocs", [], "any", false, false, false, 18), "vars", [], "any", false, false, false, 18), "prototype", [], "any", false, false, false, 18), 'widget'));
        echo "\"
            data-widget-tags=\"";
        // line 19
        echo twig_escape_filter($this->env, "<li></li>");
        echo "\"
            data-widget-counter=\"";
        // line 20
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), "children", [], "any", false, false, false, 20)), "html", null, true);
        echo "\">
          ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "imgDocs", [], "any", false, false, false, 21));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 22
            echo "              <li>
                  ";
            // line 23
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'widget');
            echo "
                  ";
            // line 24
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'errors');
            echo "
              </li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "          </ul>

          <button type=\"button\"
            class=\"add-another-collection-img btn btn-info\"
            data-list-selector=\"#img-fields-list\">Ajouter une image </button>
          </div>
          
          <div class=\"col-md-6\">
            <ul id=\"doc-fields-list\"
            data-prototype=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 36, $this->source); })()), "videoDocs", [], "any", false, false, false, 36), "vars", [], "any", false, false, false, 36), "prototype", [], "any", false, false, false, 36), 'widget'));
        echo "\"
            data-widget-tags=\"";
        // line 37
        echo twig_escape_filter($this->env, "<li></li>");
        echo "\"
            data-widget-counter=\"";
        // line 38
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), "children", [], "any", false, false, false, 38)), "html", null, true);
        echo "\">
          ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), "videoDocs", [], "any", false, false, false, 39));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 40
            echo "              <li>
                  ";
            // line 41
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'widget');
            echo "
                  ";
            // line 42
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["doc"], 'errors');
            echo "
              </li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "          </ul>

          <button type=\"button\"
            class=\"add-another-collection-video btn btn-info\"
            data-list-selector=\"#doc-fields-list\">Ajouter une vidéo </button>
          </div>
        </div>
      </div>

   
     
    <button class=\"btn btn-light-green btn-sm\">";
        // line 56
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 56, $this->source); })()), "Valider")) : ("Valider")), "html", null, true);
        echo "</button>
";
        // line 57
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 57, $this->source); })()), 'form_end');
        echo "

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "trick/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 57,  165 => 56,  152 => 45,  143 => 42,  139 => 41,  136 => 40,  132 => 39,  128 => 38,  124 => 37,  120 => 36,  109 => 27,  100 => 24,  96 => 23,  93 => 22,  89 => 21,  85 => 20,  81 => 19,  77 => 18,  68 => 12,  64 => 11,  58 => 8,  53 => 6,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{{ form_start(form) }}
  
     <div class=\"container\">
        
        <div >{{form_row(form.name)}}</div>
        
        <div>{{form_row(form.description)}}</div>   

        <div class=\"row\">
          <div class=\"col-md-6\">{{form_row(form.niveau)}}</div>
          <div class=\"col-md-6\">{{form_row(form.trick_group)}}</div>
        </div>
        <div class=\"row\">
          
          <div class=\"col-md-6\">
            <ul id=\"img-fields-list\"
            data-prototype=\"{{ form_widget(form.imgDocs.vars.prototype)|e }}\"
            data-widget-tags=\"{{ '<li></li>'|e }}\"
            data-widget-counter=\"{{ form.children|length }}\">
          {% for doc in form.imgDocs %}
              <li>
                  {{ form_widget(doc) }}
                  {{ form_errors(doc) }}
              </li>
          {% endfor %}
          </ul>

          <button type=\"button\"
            class=\"add-another-collection-img btn btn-info\"
            data-list-selector=\"#img-fields-list\">Ajouter une image </button>
          </div>
          
          <div class=\"col-md-6\">
            <ul id=\"doc-fields-list\"
            data-prototype=\"{{ form_widget(form.videoDocs.vars.prototype)|e }}\"
            data-widget-tags=\"{{ '<li></li>'|e }}\"
            data-widget-counter=\"{{ form.children|length }}\">
          {% for doc in form.videoDocs %}
              <li>
                  {{ form_widget(doc) }}
                  {{ form_errors(doc) }}
              </li>
          {% endfor %}
          </ul>

          <button type=\"button\"
            class=\"add-another-collection-video btn btn-info\"
            data-list-selector=\"#doc-fields-list\">Ajouter une vidéo </button>
          </div>
        </div>
      </div>

   
     
    <button class=\"btn btn-light-green btn-sm\">{{ button_label|default('Valider') }}</button>
{{ form_end(form) }}

", "trick/_form.html.twig", "/Users/Leila/Documents/perf/templates/trick/_form.html.twig");
    }
}
