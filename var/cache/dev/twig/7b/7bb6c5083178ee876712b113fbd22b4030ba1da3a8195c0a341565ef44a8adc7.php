<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/show.html.twig */
class __TwigTemplate_b8300f94c2c1cb1458f666d11aa3e7f3f4567969486523cc04122d06f2d56319 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'cover' => [$this, 'block_cover'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "trick/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Trick";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        echo " 
";
        // line 6
        $this->displayBlock('cover', $context, $blocks);
        // line 7
        echo "<!-- Jumbotron -->
<div class=\"card card-image\" style=\"background-image: url('";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 8, $this->source); })()), "getFirstImg", [], "any", false, false, false, 8))), "html", null, true);
        echo "'), url('";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/non-disponible.jpg"), "html", null, true);
        echo "');background-repeat: no-repeat; background-size: cover; background-position: center;\">
  <div class=\"text-white text-center rgba-stylish-strong py-5 px-4\">
    <div class=\"edit-delete-btn\">
      <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_edit", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 11, $this->source); })()), "id", [], "any", false, false, false, 11)]), "html", null, true);
        echo "\" class=\"btn btn-warning btn-sm\">edit</a>
      ";
        // line 12
        echo twig_include($this->env, $context, "member/_delete_form.html.twig");
        echo "
    </div>
    <div class=\"py-5\">

      <!-- Content -->
      <h2 class=\"h2 orange-text\"><i class=\"fas fa-snowboarding\"></i></h2>
      <h1 class=\"card-title h2 my-4 py-2\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 18, $this->source); })()), "name", [], "any", false, false, false, 18), "html", null, true);
        echo "</h1>

    </div>
  </div>
</div>
<!-- Jumbotron -->

<div class=\"docs-container row text-center\"> 

 ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 27, $this->source); })()), "getImgDocs", [], "method", false, false, false, 27));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 28
            echo " <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
    <a><img src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" class=\"thumbnails-docs\" data-toggle=\"modal\" data-target=\"#modal";
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\"></a>


  <!--Modal: modalYT-->
  <div class=\"modal fade\" id=\"modal";
            // line 33
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"";
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">

      <!--Content-->
      <div class=\"modal-content\">

        <!--Body-->
        <div class=\"modal-body mb-0 p-0\">

          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
              <img src=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" class=\"embed-responsive-item\">
          </div>
        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: modalYT-->
 </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "
  ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 56, $this->source); })()), "getVideoDocs", [], "method", false, false, false, 56));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 57
            echo " <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
    <a><img src=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/video-icon.jpg"), "html", null, true);
            echo "\" class=\"thumbnails-docs\" data-toggle=\"modal\" data-target=\"#modal";
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\"></a>


  <!--Modal: modalYT-->
  <div class=\"modal fade\" id=\"modal";
            // line 62
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"";
            echo twig_escape_filter($this->env, twig_first($this->env, twig_split_filter($this->env, $context["file"], ".")), "html", null, true);
            echo "\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">

      <!--Content-->
      <div class=\"modal-content\">

        <!--Body-->
        <div class=\"modal-body mb-0 p-0\">

          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">

            <video  class=\"embed-responsive-item\" controls>
              <source src=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . $context["file"])), "html", null, true);
            echo "\" type=\"video/mp4\" allowfullscreen>
            </video>

          </div>
        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: modalYT-->
 </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "
</div>


<!-- Card -->
<div class=\"card card-cascade wider reverse\">

  <!-- Card content -->
  <div class=\"card-body card-body-cascade text-center\">

    <!-- Title -->
    <h4 class=\"card-title\"><strong>Description</strong></h4>
    <!-- Text -->
    <p class=\"card-text\">";
        // line 101
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 101, $this->source); })()), "description", [], "any", false, false, false, 101), "html", null, true);
        echo "</p>
    <hr>
    <div class=\"trick-info-collapse\">
      <p>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample1\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Niveau</button>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample2\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Catégorie</button>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample3\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Date d'ajout</button>
    </p>
    <div class=\"row\">
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample1\">
          <div class=\"card card-body alert alert-dark\">
            ";
        // line 113
        echo twig_escape_filter($this->env, (isset($context["niveau"]) || array_key_exists("niveau", $context) ? $context["niveau"] : (function () { throw new RuntimeError('Variable "niveau" does not exist.', 113, $this->source); })()), "html", null, true);
        echo "
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample2\">
          <div class=\"card card-body alert alert-dark\">
            ";
        // line 120
        echo twig_escape_filter($this->env, (isset($context["trick_group"]) || array_key_exists("trick_group", $context) ? $context["trick_group"] : (function () { throw new RuntimeError('Variable "trick_group" does not exist.', 120, $this->source); })()), "html", null, true);
        echo "
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample3\">
          <div class=\"card card-body alert alert-dark\">
            ";
        // line 127
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 127, $this->source); })()), "datecreation", [], "any", false, false, false, 127), "d/m/Y"), "html", null, true);
        echo "
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>

</div>
<!-- Card -->

<!-- Comments input-->

";
        // line 140
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 141
            echo "<div class=\"comments-container\">
  <div>
    ";
            // line 143
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 143, $this->source); })()), 'form_start');
            echo "

      <div class=\"comment-input text-center\">
       ";
            // line 146
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 146, $this->source); })()), "content", [], "any", false, false, false, 146), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "

       <button class=\"btn btn-indigo\" id=\"jq-insert\">Envoyer</button> 
      </div>
    ";
            // line 150
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 150, $this->source); })()), 'form_end');
            echo "
  </div>

  ";
        }
        // line 154
        echo "<!-- Comments input-->


<!-- Comments Display-->
  <div id=\"comments_block\" class=\"text-center\">

    
  </div>
<!-- Comments Display-->
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_cover($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 167
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 168
        echo "
  <script type=\"text/javascript\">
    
    \$(document).ready(function(){
      \$.post( \"";
        // line 172
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_comments", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 172, $this->source); })()), "id", [], "any", false, false, false, 172)]), "html", null, true);
        echo "\")
        .done(function(comments){
          \$('#comments_block').empty().append(comments);

          \$('#jq-plus').on('click', function(){
        var number_com = \$('#jq-com > div').length;
      \$.post( \"";
        // line 178
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_comments", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["trick"]) || array_key_exists("trick", $context) ? $context["trick"] : (function () { throw new RuntimeError('Variable "trick" does not exist.', 178, $this->source); })()), "id", [], "any", false, false, false, 178)]), "html", null, true);
        echo "\", {first: number_com})
        .done(function(comms){
          \$(comms).insertBefore(\"#jq-plus\");
          if (!\$(comms).hasClass(\"more\")) {
              \$(\"#jq-plus\").hide();
          }
          // SI LE RESULTAT DE LA REQUETE EST VIDE OU QU IL Y A MOINS DE 3 TRICKS CACHER LE BUTTON

        })
        

    });
        }) 

      });

  </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "trick/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  391 => 178,  382 => 172,  376 => 168,  366 => 167,  348 => 6,  327 => 154,  320 => 150,  313 => 146,  307 => 143,  303 => 141,  301 => 140,  285 => 127,  275 => 120,  265 => 113,  250 => 101,  235 => 88,  215 => 74,  198 => 62,  189 => 58,  186 => 57,  182 => 56,  179 => 55,  161 => 43,  146 => 33,  137 => 29,  134 => 28,  130 => 27,  118 => 18,  109 => 12,  105 => 11,  97 => 8,  94 => 7,  92 => 6,  80 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Trick{% endblock %}

{% block body %} 
{% block cover %}{% endblock %}
<!-- Jumbotron -->
<div class=\"card card-image\" style=\"background-image: url('{{ asset('uploads/images/' ~ trick.getFirstImg)}}'), url('{{ asset('img/non-disponible.jpg')}}');background-repeat: no-repeat; background-size: cover; background-position: center;\">
  <div class=\"text-white text-center rgba-stylish-strong py-5 px-4\">
    <div class=\"edit-delete-btn\">
      <a href=\"{{ path('trick_edit', {'id': trick.id})}}\" class=\"btn btn-warning btn-sm\">edit</a>
      {{ include('member/_delete_form.html.twig') }}
    </div>
    <div class=\"py-5\">

      <!-- Content -->
      <h2 class=\"h2 orange-text\"><i class=\"fas fa-snowboarding\"></i></h2>
      <h1 class=\"card-title h2 my-4 py-2\">{{ trick.name }}</h1>

    </div>
  </div>
</div>
<!-- Jumbotron -->

<div class=\"docs-container row text-center\"> 

 {% for file in trick.getImgDocs() %}
 <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
    <a><img src=\"{{ asset('uploads/images/' ~ file)}}\" class=\"thumbnails-docs\" data-toggle=\"modal\" data-target=\"#modal{{(file|split('.')|first)}}\"></a>


  <!--Modal: modalYT-->
  <div class=\"modal fade\" id=\"modal{{(file|split('.')|first)}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"{{(file|split('.')|first)}}\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">

      <!--Content-->
      <div class=\"modal-content\">

        <!--Body-->
        <div class=\"modal-body mb-0 p-0\">

          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">
              <img src=\"{{ asset('uploads/images/' ~ file)}}\" class=\"embed-responsive-item\">
          </div>
        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: modalYT-->
 </div>
  {% endfor %}

  {% for file in trick.getVideoDocs() %}
 <div class=\"col-sm-6 col-md-4 col-lg-3 m-0 p-3\">
    <a><img src=\"{{ asset('img/video-icon.jpg')}}\" class=\"thumbnails-docs\" data-toggle=\"modal\" data-target=\"#modal{{(file|split('.')|first)}}\"></a>


  <!--Modal: modalYT-->
  <div class=\"modal fade\" id=\"modal{{(file|split('.')|first)}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"{{(file|split('.')|first)}}\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">

      <!--Content-->
      <div class=\"modal-content\">

        <!--Body-->
        <div class=\"modal-body mb-0 p-0\">

          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">

            <video  class=\"embed-responsive-item\" controls>
              <source src=\"{{ asset('uploads/images/' ~ file)}}\" type=\"video/mp4\" allowfullscreen>
            </video>

          </div>
        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: modalYT-->
 </div>
  {% endfor %}

</div>


<!-- Card -->
<div class=\"card card-cascade wider reverse\">

  <!-- Card content -->
  <div class=\"card-body card-body-cascade text-center\">

    <!-- Title -->
    <h4 class=\"card-title\"><strong>Description</strong></h4>
    <!-- Text -->
    <p class=\"card-text\">{{trick.description}}</p>
    <hr>
    <div class=\"trick-info-collapse\">
      <p>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample1\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Niveau</button>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample2\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Catégorie</button>
      <button class=\"btn btn-info\" type=\"button\" data-toggle=\"collapse\" data-target=\"#multiCollapseExample3\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample2\">Date d'ajout</button>
    </p>
    <div class=\"row\">
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample1\">
          <div class=\"card card-body alert alert-dark\">
            {{niveau}}
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample2\">
          <div class=\"card card-body alert alert-dark\">
            {{trick_group}}
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"collapse multi-collapse\" id=\"multiCollapseExample3\">
          <div class=\"card card-body alert alert-dark\">
            {{trick.datecreation|date('d/m/Y')}}
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>

</div>
<!-- Card -->

<!-- Comments input-->

{% if is_granted('IS_AUTHENTICATED_FULLY') %}
<div class=\"comments-container\">
  <div>
    {{form_start(form)}}

      <div class=\"comment-input text-center\">
       {{form_widget(form.content, { 'attr': {'class': 'form-control'} })}}

       <button class=\"btn btn-indigo\" id=\"jq-insert\">Envoyer</button> 
      </div>
    {{form_end(form)}}
  </div>

  {% endif %}
<!-- Comments input-->


<!-- Comments Display-->
  <div id=\"comments_block\" class=\"text-center\">

    
  </div>
<!-- Comments Display-->
</div>

{% endblock %}

{% block javascripts %}

  <script type=\"text/javascript\">
    
    \$(document).ready(function(){
      \$.post( \"{{path('new_comments', {id:trick.id})}}\")
        .done(function(comments){
          \$('#comments_block').empty().append(comments);

          \$('#jq-plus').on('click', function(){
        var number_com = \$('#jq-com > div').length;
      \$.post( \"{{path('new_comments',{id:trick.id})}}\", {first: number_com})
        .done(function(comms){
          \$(comms).insertBefore(\"#jq-plus\");
          if (!\$(comms).hasClass(\"more\")) {
              \$(\"#jq-plus\").hide();
          }
          // SI LE RESULTAT DE LA REQUETE EST VIDE OU QU IL Y A MOINS DE 3 TRICKS CACHER LE BUTTON

        })
        

    });
        }) 

      });

  </script>

{% endblock %}
", "trick/show.html.twig", "/Users/Leila/Documents/perf/templates/trick/show.html.twig");
    }
}
